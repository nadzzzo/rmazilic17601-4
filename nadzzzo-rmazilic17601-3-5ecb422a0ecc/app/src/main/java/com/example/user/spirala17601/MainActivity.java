package com.example.user.spirala17601;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ba.unsa.nzilic1etf.spirala.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
