package ba.unsa.nzilic1etf.spirala;

import android.Manifest;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by User on 9.6.2017.
 */

public class fragment_detalji_ofilmu extends Fragment {
     public static final int REZULTAT=0;
    boolean wtf = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.detalji_o_filmu_fragment, container, false);

        final TextView nameee = (TextView) v.findViewById(R.id.textView3);
        final DatePicker datum = (DatePicker) v.findViewById(R.id.datePicker2);
        final EditText detalji = (EditText) v.findViewById(R.id.editText3);
        Button zapamtidgm = (Button) v.findViewById(R.id.button6);
        String film = "";
        if (getArguments() != null && getArguments().containsKey("film")) {


            film= getArguments().getString("film");
            nameee.setText(film);
        }


        zapamtidgm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentResolver cr = getActivity().getApplicationContext().getContentResolver();
                ContentValues cv = new ContentValues();


                Calendar beginTime = Calendar.getInstance();
                int god = datum.getYear();
                int mj = datum.getMonth();
                int dan = datum.getDayOfMonth();
                beginTime.set(god,mj,dan);
                long endDate = beginTime.getTimeInMillis() + 1000 * 60 * 60;

                cv.put(CalendarContract.Events.DTSTART, beginTime.getTimeInMillis());
                cv.put(CalendarContract.Events.DTEND, endDate);
                cv.put(CalendarContract.Events.TITLE, nameee.getText().toString());
                cv.put(CalendarContract.Events.DESCRIPTION, detalji.getText().toString());
                cv.put(CalendarContract.Events.CALENDAR_ID, 1);
                cv.put(CalendarContract.Events.HAS_ALARM, 1);
                cv.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());
                cr.insert(CalendarContract.Events.CONTENT_URI, cv);

                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED)
                {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_CALENDAR}, REZULTAT);
                }
                else
                {
                    povezi();
                }

                Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, cv);
                Toast.makeText(getActivity(), "Događaj je evidentiran! ", Toast.LENGTH_LONG).show();
            }
        });

            return v;
        }
    private int povezi() {
        ContentResolver cr = getActivity().getApplicationContext().getContentResolver();

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {

            return 0;
        }
        Cursor c = cr.query(CalendarContract.Calendars.CONTENT_URI, new String[]{CalendarContract.CalendarEntity._ID, CalendarContract.CalendarEntity.CALENDAR_DISPLAY_NAME}, null, null, null);

        while (c.moveToNext()) {
            int calendarID = c.getInt(c.getColumnIndex(CalendarContract.CalendarEntity._ID));

        }
        return -1;

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REZULTAT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    wtf = true;
                    Toast.makeText(getActivity(), "uspjelo",
                            Toast.LENGTH_LONG).show();

                } else {
                    wtf = false;
                    Toast.makeText(getActivity(), "bas si beze",
                            Toast.LENGTH_LONG).show();

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    }

