package ba.unsa.nzilic1etf.spirala;

import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Main2Activity extends AppCompatActivity {
    ImageView slikaGl;
    TextView imeGl;
    TextView prezimeGl;
    TextView godRodjGl;
    TextView godSmrtiGl;
    TextView mjestoRodjGl;
    TextView spolGl;
    TextView bioGl;
    TextView linkGl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        Button dugmepodijeli=(Button) findViewById(R.id.button4podijeli);
        slikaGl =(ImageView) findViewById(R.id.imageView);
        imeGl=(TextView) findViewById(R.id.textViewIGl);
        prezimeGl=(TextView) findViewById(R.id.textViewPGl);
        godRodjGl=(TextView) findViewById(R.id.textViewGRGl);
        godSmrtiGl=(TextView) findViewById(R.id.textViewGSGl);
        mjestoRodjGl=(TextView) findViewById(R.id.textViewMRGl);
        spolGl=(TextView) findViewById(R.id.textViewSGl);
        bioGl=(TextView) findViewById(R.id.textViewBGl);
        linkGl=(TextView) findViewById(R.id.textViewLGl);
       RelativeLayout bgElement = (RelativeLayout) findViewById(R.id.glavni);

        //bgElement.setBackgroundColor(Color.BLUE);




        imeGl.setText(getIntent().getStringExtra("imeGl"));
        prezimeGl.setText(getIntent().getStringExtra("prezimeGl"));
        godRodjGl.setText(getIntent().getStringExtra("godinaRodjenjaGl"));
        godSmrtiGl.setText(getIntent().getStringExtra("godinaSmrtiGl"));
        mjestoRodjGl.setText(getIntent().getStringExtra("mjestoRodjenjaGl"));
        spolGl.setText(getIntent().getStringExtra("spolGl"));
        bioGl.setText(getIntent().getStringExtra("bioGl"));
        linkGl.setText(getIntent().getStringExtra("linkGl"));
       // slikaGl.setImageResource();
       slikaGl.setImageResource(Integer.parseInt(getIntent().getStringExtra("slikaGl")));

        String spol = (String)spolGl.getText();

        if((spol).equals("Female")) {

            bgElement.setBackgroundColor(Color.parseColor("#a0f9a4f2"));

        }
        else if((spol).equals("Male"))
        {
            bgElement.setBackgroundColor(Color.parseColor("#97dafc"));

        }
        linkGl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentlink=new Intent(Intent.ACTION_WEB_SEARCH);
                intentlink.putExtra(SearchManager.QUERY,linkGl.getText());
            startActivity(intentlink);}

        });

        dugmepodijeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, bioGl.getText());
                sendIntent.setType("text/plain");
// Provjera da li postoji aplikacija koja može obaviti navedenu akciju
                if (sendIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(sendIntent);

                }
            }
        });



    }
}
