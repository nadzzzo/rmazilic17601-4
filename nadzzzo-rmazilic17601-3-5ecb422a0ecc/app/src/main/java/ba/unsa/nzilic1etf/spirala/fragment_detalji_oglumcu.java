package ba.unsa.nzilic1etf.spirala;

import android.app.Fragment;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by User on 10.4.2017.
 */

public class fragment_detalji_oglumcu extends Fragment  {
    private Glumac glumac;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.detalji_o_glumcu_fragment, container, false);
        if(getArguments()!= null && getArguments().containsKey("glumac"))
        {
            glumac = getArguments().getParcelable("glumac");
            TextView ime = (TextView)view.findViewById(R.id.textViewIGl);
            TextView prezime = (TextView)view.findViewById(R.id.textViewPGl);
            TextView godinarodjenja = (TextView)view.findViewById(R.id.textViewGRGl);
            TextView godinasmrti = (TextView)view.findViewById(R.id.textViewGSGl);
            TextView mjestorodjenja = (TextView)view.findViewById(R.id.textViewMRGl);
            TextView biografija = (TextView)view.findViewById(R.id.textViewBGl);
            TextView spol = (TextView)view.findViewById(R.id.textViewSGl);
            ImageView slika = (ImageView)view.findViewById(R.id.imageView);
            RelativeLayout bgElement = (RelativeLayout) view.findViewById(R.id.glavnilayout);
            Button bookmark = (Button)view.findViewById(R.id.buttonSpasi);
            Button delete = (Button)view.findViewById(R.id.buttonBrisi);
            // Dodala sam ovo dugme za prikaz jer još uvijek nisam uradila čitanje iz baze pa da mogu ovako pregledati sadržaj
            Button prikazi = (Button) view.findViewById(R.id.buttonPrikazi);

            String spolglumca = (String)glumac.getSpol();
            //bgElement.setBackgroundColor(Color.parseColor("#a0f9a4f2"));

            bookmark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DataBaseAdapter base = new DataBaseAdapter(getActivity());
                    String reziserii = "";
                    String zanrovii = null;
                    for(int i = 0; i<glumac.getReziseri().size(); i++)
                    {
                       ArrayList<String> rez =  glumac.getReziseri();
                        reziserii += rez.get(i) + ",";

                    }
                    for(int i = 0; i<glumac.getZanrovi().size(); i++)
                    {
                        ArrayList<Zanr> rez =  glumac.getZanrovi();
                        zanrovii += rez.get(i).getNaziv() + ",";

                    }


                    base.open();
                    base.insertGlumac(
                            glumac.getIme(),
                            glumac.getGodinaRodjenja(),
                            glumac.getGodinaSmrti(),
                            glumac.getMjestoRodjenja(),
                            glumac.getSpol(),
                            glumac.getSlika(),
                            glumac.getBiografija(),
                            glumac.getLink(),
                            glumac.getRating(),
                             reziserii,
                            zanrovii);
                   base.close();
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*final String imegl= glumac.getIme();
            izbrisi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    DBAdapter pom= new DBAdapter(getActivity());
                    pom.open();
                    Cursor cursor= pom.getAllActors();
                    if(cursor.moveToFirst())
                    {
                        do{
                            if(imegl.contains(cursor.getString(2)))
                            {
                                boolean a= pom.deleteglumcabyname(glumac.getIme());
                                if(a)
                                po.porukica(getActivity(), "Izbrisan");

                            }

                        }while(cursor.moveToNext());
                    }
                    pom.close();
                    cursor.close();

                }
            });*/

                  /*  String imegl = glumac.getIme();
                    boolean imaga=false;
                    DataBaseAdapter dbEntry = new DataBaseAdapter(getActivity());
                    dbEntry.open();
                    Cursor c= dbEntry.getAllActors();
                    if(c.moveToFirst())
                    {
                        do {
                            if(imegl.contains(c.getString(1)))
                            {
                                imaga=true;
                                if(imaga)
                                {
                                    dbEntry.deleteglumcabyname(imegl);
                                    Toast.makeText(getActivity(), "Izbrisan", Toast.LENGTH_LONG).show();
                                }
                                if(!imaga)
                                {
                                    Toast.makeText(getActivity(), "Ne postoji", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                        while(c.moveToNext());

                    }
                    c.close();
                    dbEntry.close(); */
                    String imegl = glumac.getIme()+" " + glumac.getPrezime();
                    DataBaseAdapter pom= new DataBaseAdapter(getActivity());
                    pom.open();
                    Cursor cursor= pom.getAllActors();
                    if(cursor.moveToFirst())
                    {
                        do{
                            if(imegl.contains(cursor.getString(1)))
                             {
                                boolean a= pom.deleteglumcabyname(imegl);
                                if(a)
                                    Toast.makeText(getActivity(), "Izbrisan", Toast.LENGTH_LONG).show();

                            }

                        }while(cursor.moveToNext());
                    }
                    pom.close();
                    cursor.close();

                }
            });

            prikazi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    DataBaseAdapter pom= new DataBaseAdapter(getActivity());
                    pom.open();
                    Cursor injih= pom.getAllActors();
                    if(injih.moveToFirst())
                    {

                        do{

                            Toast.makeText(getActivity(), injih.getString(1),
                                    Toast.LENGTH_LONG).show();
                        }while(injih.moveToNext());
                    }
                    pom.close();
                    injih.close();
                }
                    /* if(pregledaj.moveToFirst())
                    {
                        do{
                           pom.Display(pregledaj);
                           //   pom.Display(injih);
                        }while(pregledaj.moveToNext());
                    }
                    pom.close();
                    pregledaj.close();
           */

            });



            if((spolglumca).equals("Female")) {

                bgElement.setBackgroundColor(Color.parseColor("#a0f9a4f2"));

            }
            else if((spolglumca).equals("Male"))
            {
                bgElement.setBackgroundColor(Color.parseColor("#97dafc"));

            }
            ime.setText(glumac.getIme());
            prezime.setText(glumac.getPrezime());
            godinarodjenja.setText(glumac.getGodinaRodjenja());
            godinasmrti.setText(glumac.getGodinaSmrti());
            mjestorodjenja.setText(glumac.getMjestoRodjenja());
            biografija.setText(glumac.getBiografija());
            spol.setText(glumac.getSpol());

            Picasso.with(container.getContext()).load("https://image.tmdb.org/t/p/original/"+glumac.getSlika()).into(slika);
            //slika.setImageResource(glumac.getSlika());

        }
        return view;


    }

}
