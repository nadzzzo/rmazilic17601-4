package ba.unsa.nzilic1etf.spirala;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by User on 10.4.2017.
 */

public class fragment_lista_zanrova extends Fragment {
    public static ArrayList<Zanr> zanrovi = new ArrayList<>();
    public static AdapterListeRezisera adapter = null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lista_zanrova_fragment, container, false);


        ListView lista = (ListView) view.findViewById(R.id.listViewZANROVI);



        if(getArguments()!= null && getArguments().containsKey("zanrovi")) {

            Glumac g = getArguments().getParcelable("zanrovi");
            zanrovi = g.getZanrovi();
        }



        final AdapterListeZanrova adapter = new AdapterListeZanrova(getActivity(), zanrovi, this.getResources());
        lista.setAdapter(adapter);
        return view;


    }

    class Adapter1 extends ArrayAdapter<String>

    {
        Context contekst;
        int[] slike;
        String[] nazivi;


        Adapter1(Context c, int s[], String[] im) {
            super(c, R.layout.zanrulisti, R.id.tekstViewIKONA, im);
            this.contekst = c;
            this.nazivi = im;
            this.slike = s;
        }

    }
}