package ba.unsa.nzilic1etf.spirala;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Created by User on 23.5.2017.
 */

public class ResiverZaRezultat extends ResultReceiver {

    private Receiver pomocni;

    public ResiverZaRezultat(Handler handler) {
        super(handler);
    }

    public void setReceiver(Receiver receiver) {
        pomocni = receiver;
    }

    public interface Receiver
    {
        public void onReceiveResult(int resultCode, Bundle resultData);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData)
    {
        if(pomocni != null)
        {
            pomocni.onReceiveResult(resultCode,resultData);
        }
    }
}
