package ba.unsa.nzilic1etf.spirala;

import android.content.Intent;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by User on 22.5.2017.
 */
public class SearchActor extends AsyncTask<String, Integer, Void> {


    public interface OnMuzicarSearchDone {
        public void onDone(ArrayList<Glumac> rez);
    }

    Glumac rez;
    private OnMuzicarSearchDone pozivatelj;
    public SearchActor(OnMuzicarSearchDone p) {pozivatelj=p;};




    @Override
    protected Void doInBackground(String... params) {

        String query = null;
        try {
            query = URLEncoder.encode(params[0], "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
        }

        String url1 = "https://api.themoviedb.org/3/search/person?api_key=04238c25b41882a87b8d61e6d14cbb6d&query" + query;
        try {
            URL url = new URL(url1);

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty("User-Agent","Chrome");
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);


            JSONArray items = jo.getJSONArray("results");


            for (int i = 0; i < items.length(); i++) {
                JSONObject artist = items.getJSONObject(i);
                String name = artist.getString("name");
                String artist_ID = artist.getString("id");
                //rez=new Glumac(name, "zanr", "webpage");OVDJEEEE


            }
        } catch (
                MalformedURLException e)

        {
            e.printStackTrace();
        } catch (
                IOException e)

        {
            e.printStackTrace();
        } catch (
                JSONException e)

        {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        //pozivatelj.onDone(rez);
    }


    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }



       /* @Override
        public void onDone (Glumac m) {
            muzicar = m;
            ime.setText(muzicar.getIme());
        }


        Button dgm=(Button) iv.findViewById(R.id.button2);
    dgm.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick (View v){
            new FragmentGlumci.SearchArtist((FragmentGlumci.SearchArtist.OnMuzicarSearchDone)
                    FragmentGlumci.this).execute(ime_koje_trazim);
        }
        }

        new SearchArtist().execute(upit);*/
}