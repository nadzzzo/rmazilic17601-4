package ba.unsa.nzilic1etf.spirala;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by User on 28.3.2017.
 */

public class AdapterListeZanrova extends BaseAdapter{
    private Activity aktivnost;
    private ArrayList podaci;
    private static LayoutInflater inflater = null;
    public Resources res;
    Zanr privVrijednosti = null;
    int i = 0;

    public AdapterListeZanrova(Activity a, ArrayList e, Resources res) {
        this.aktivnost = a;
        this.podaci = e;
        this.res = res;
        inflater = (LayoutInflater) aktivnost.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        if (podaci.size() <= 0) return 1;
        return podaci.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class PrikazReda {

        public ImageView ikona;
        public TextView naziv;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View pogled = convertView;
        AdapterListeZanrova.PrikazReda prikaz;

        if (convertView == null) {
            pogled = inflater.inflate(R.layout.zanrulisti, null);

            prikaz = new AdapterListeZanrova.PrikazReda();

            prikaz.ikona = (ImageView) pogled.findViewById(R.id.imageViewIKONA);
            prikaz.naziv = (TextView) pogled.findViewById(R.id.tekstViewIKONA);


            pogled.setTag(prikaz);

        } else prikaz = (AdapterListeZanrova.PrikazReda) pogled.getTag();

        if (podaci.size() <= 0) {
            prikaz.naziv.setText("No data");

        } else {
            privVrijednosti = null;
            privVrijednosti = (Zanr) podaci.get(position);
            prikaz.ikona.setImageResource(privVrijednosti.getIkona());
           // prikaz.ikona.setImageResource(res.getIdentifier("ba.unsa.nzilic1etf.spirala:drawable/" + privVrijednosti.getIkona(), null, null));
            prikaz.naziv.setText(privVrijednosti.getNaziv());


        }
        return pogled;
    }
}
