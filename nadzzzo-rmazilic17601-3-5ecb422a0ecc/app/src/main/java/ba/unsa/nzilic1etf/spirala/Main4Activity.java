package ba.unsa.nzilic1etf.spirala;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import static ba.unsa.nzilic1etf.spirala.R.id.parent;

public class Main4Activity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        Resources res = getResources();


        ListView lista = (ListView) findViewById(R.id.listViewZANROVI);

        final ArrayList<Zanr> zanrovi = new ArrayList<Zanr>();

      /*  zanrovi.add(new Zanr("action", "Akcija"));
        zanrovi.add(new Zanr("love", "Ljubavni"));
        zanrovi.add(new Zanr("horror", "Horor"));
        zanrovi.add(new Zanr("scifi", "Sci-fi"));
        zanrovi.add(new Zanr("vestern", "Vestern"));
        zanrovi.add(new Zanr("porodicni", "Porodični"));
        zanrovi.add(new Zanr("komedija", "Komedija"));
        zanrovi.add(new Zanr("crtani", "Crtani"));*/


        final AdapterListeZanrova adapter = new AdapterListeZanrova(this, zanrovi, this.getResources());
        lista.setAdapter(adapter);

/*
        dugmeGlumci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentR = new Intent(Main4Activity.this, MainActivity.class);
                startActivity(intentR);
            }
        });

        dugmeReziseri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentZ = new Intent(Main4Activity.this, Main3Activity.class);
                startActivity(intentZ);
            }
        });*/

    }

    class Adapter1 extends ArrayAdapter<String> {
        Context contekst;
        int[] slike;
        String[] nazivi;


        Adapter1(Context c, int s[], String[] im) {
            super(c, R.layout.zanrulisti, R.id.tekstViewIKONA, im);
            this.contekst = c;
            this.nazivi = im;
            this.slike = s;
        }


            @Override
            public View getView ( int position, View convertView, ViewGroup parent)
            {
                LayoutInflater inf = (LayoutInflater) contekst.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View red = inf.inflate(R.layout.zanrulisti, parent, false);
                ImageView slikaikone = (ImageView) red.findViewById(R.id.imageViewIKONA);
                TextView nazivikone = (TextView) red.findViewById(R.id.tekstViewIKONA);

                slikaikone.setImageResource(slike[position]);
                nazivikone.setText(nazivi[position]);


                return red;
            }
        }

    }
