package ba.unsa.nzilic1etf.spirala;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements FragmentChangeListener, fragment_lista_glumaca.OnItemClick, fragment_filmovi.OnItemClick {
    public ArrayList<Glumac> glumci = new ArrayList<Glumac>();
    public ArrayList<String> film_staticki = new ArrayList<>();
    public Boolean trifragmenta;
    public String koji;
    FragmentTransaction transaction;
    fragment_dugmad myf;
    Glumac trenutni;
    Integer last_index;
    public fragment_lista_rezisera fr = new fragment_lista_rezisera();
    public static DataBaseAdapter.GlumciDataBOpenHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
     //   Stetho.initializeWithDefaults(this);

        setContentView(R.layout.activity_main);
        db = new DataBaseAdapter.GlumciDataBOpenHelper(this);




       /* int megan = R.drawable.meganfox;
        int adam = R.drawable.adamsandler;
        int angelina = R.drawable.angelinajolie;
        int antonio = R.drawable.antoniobanderas;
        int ben = R.drawable.benaffleck;
        int cameron = R.drawable.camerondiaz;
        int ashton = R.drawable.ashtonkutcher;

        Glumac meganfox = new Glumac(megan, "Megan", "Fox", "1986", "Rockwood", "25", "Male", "/", "Megan Fox (Rockwood, Tennessee, 16. svibnja 1986.) američka je glumica i model, irskih, talijanskih, francuskih i čerokijskih korijena. Počela je pohađati satove glume i plesa s pet godina. Kad je imala deset godina, preselila se na Floridu, gdje je završila školu, nakon čega je, zbog posla modela, otišla u Los Angeles.\n" +
                "\n" +
                "Na filmu je debitirala 2001. godine ulogom u filmu Holiday in the Sun. Najpoznatija je po ulogama u filmovima Transformeri i Transformeri: Osveta poraženih, u kojima glumi uz Shiu LaBeoufa, kao i po ulozi u TV seriji Moja slavna sestra.", "http://www.imdb.com/name/nm1083271/");
        glumci.add(meganfox);
        Glumac adamsandler = new Glumac(adam, "Adam", "Sandler", "1966", "New York", "18", "Male", "/", "dam Richard Sandler (Brooklyn, New York, 9. rujna 1966.) je američki komičar, glumac, scenarist i glazbenik.\n" +
                "\n" +
                "Karijeru je počeo u američkoj emisiji zvanoj Saturday Night Live (hrv. Subotnja Večer Uživo). Najpoznatije uloge su mu u filmovima Tata od formata, Luda vjenčanja i Vodonoša, a poznat je po još važnih uloga.", "http://www.imdb.com/name/nm0001191/");
        glumci.add(adamsandler);
        Glumac angelinajolie = new Glumac(angelina, "Angelina", "Jolie", "1975", "Los Angeles", "1", "Female", "/", "Angelina Jolie, rođena Angelina Jolie Voight (Los Angeles, Kalifornija, 4. lipnja 1975.), američka filmska glumica. Dobitnica je tri Zlatna globusa i Oskara za najbolju sporednu ulogu.\n" +
                "\n" +
                "Godine 2006. izabrana je za \"najzgodnije slavno tijelo\" u izboru E! Television i najljepšu ženu u izboru časopisa \"People\".[1] Redovito je visoko rangirana na takvim izborima.\n" +
                "\n" +
                "UNHCR-ova je veleposlanica dobre volje", "http://www.imdb.com/name/nm0001401/");
        glumci.add(angelinajolie);

        Glumac antoniobanderas = new Glumac(antonio, "Antonio", "Banderas", "1960", "Malaga", "56", "Male", "/", "José Antonio Domínguez Banderas (Malaga, 10. kolovoza 1960.) je španjolski filmski glumac, redatelj, producent i pjevač. Istakao se glumeći u holivudskim filmovima kao što su: Ubojice, Intervju s vampirom, Desperado, Philadelphia i Zorro: Maskirani osvetnik.", "http://www.imdb.com/name/nm0000104/");
        glumci.add(antoniobanderas);
        Glumac ashtonkutcher = new Glumac(ashton, "Ashton", "Kuthcer", "1978", "Iowa", "52", "Male", "/", "Christopher Ashton Kutcher (Cedar Rapids, Iowa, 7. veljače 1978.), američki filmski i televizijski glumac, producent i bivši model. Najpoznatiji je po ulozi Michaela Kelsa u humorističnoj seriji Lude sedamdesete i po braku sa starijom glumicom Demi Moore.\n" +
                "\n" +
                "Godine 2010. je, zajedno sa suprugom Demi Moore, osnovao zakladu za iskorjenjivanje dječjeg ropstva u svijetu.[1] Iste godine, Time Magazine proglasio ga je jednim od 100 najutjecajnijih ljudi", "http://www.imdb.com/name/nm0005110/");
        glumci.add(ashtonkutcher);
        Glumac benaffleck = new Glumac(ben, "Ben", "Affleck", "1972", "Kalifornija", "4", "Male", "/", "Ben Affleck (Berkeley, Kalifornija, 15. kolovoza 1972.), američki glumac.", "http://www.imdb.com/name/nm0000255/");
        glumci.add(benaffleck);
        Glumac camerondiaz = new Glumac(cameron, "Cameron", "Diaz", "1972", "San Diego", "35", "Female", "/", "Sa 16 godina otišla je od kuće i radila kao foto model, a u sljedećih je pet godina živjela na raznim lokacijama po cijelom svijetu. Iako bez glumačkog iskustva, 1994. godine debitira na filmu glavnom ženskom ulogom uz Jima Carreyja u komediji Maska. Nakon nekoliko uloga u nezavisnim i niskobudžetnim filmovima popularnost joj raste 1998. godine kada igra glavnu ulogu u romantičnoj komediji Svi su ludi za Mary. Dvije godine kasnije igra glavnu ulogu u Charliejevim anđelima, a 2001. nastupa uz Toma Cruisea u Nebu boje vanilije, te daje glas princezi Fioni u popularnom animiranom filmu Shrek.", "http://www.imdb.com/name/nm0000139/");
        glumci.add(camerondiaz);*/
        Boolean var = false;

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {

            myf = new fragment_dugmad();
            fragment_lista_glumaca lr = new fragment_lista_glumaca();
            Bundle argumenti = new Bundle();
            argumenti.putParcelableArrayList("Alista", glumci);
            lr.setArguments(argumenti);
            transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.mjestoF1, myf);
            transaction.replace(R.id.mjestoF2, lr);
            transaction.commit();
        } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            //transaction.remove()

            Button buttonzaglumce = (Button) findViewById(R.id.buttonzaglumce);
            Button buttonzaostalo = (Button) findViewById(R.id.buttonzaostalo);

            fragment_lista_glumaca fragglumci = new fragment_lista_glumaca();
            fragment_detalji_oglumcu detalji = new fragment_detalji_oglumcu();
            Bundle argumenti = new Bundle();
            argumenti.putParcelableArrayList("Alista", glumci);
            fragglumci.setArguments(argumenti);

            transaction = getFragmentManager().beginTransaction();

            buttonzaglumce.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    fragment_lista_glumaca fragglumci = new fragment_lista_glumaca();
                    Bundle argumenti = new Bundle();
                    argumenti.putParcelableArrayList("Alista", glumci);
                    fragglumci.setArguments(argumenti);
                    transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.mjestoZaGlumce, fragglumci).addToBackStack(null);
                    transaction.commit();

                }
            });
            buttonzaostalo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  /*   fragment_lista_rezisera rez = new fragment_lista_rezisera();
                     fragment_lista_zanrova zanr = new fragment_lista_zanrova();
                     transaction = getFragmentManager().beginTransaction();
                     transaction.replace(R.id.mjestoZaGlumce, zanr);
                     transaction.replace(R.id.mjestoZaOstalo,rez);
                     transaction.commit();
                     */

                    FragmentManager fm = getFragmentManager();
                    Fragment a = fm.findFragmentById(R.id.mjestoZaOstalo);
                    if (!(a instanceof fragment_lista_rezisera)) {
                        fragment_lista_rezisera fr = (fragment_lista_rezisera) fm.findFragmentById(R.layout.lista_rezisera_fragment);
                        if (fr == null) {
                            fr = new fragment_lista_rezisera();
                            if (last_index != -1) {
                                Bundle argumenti = new Bundle();
                                argumenti.putParcelable("reziseri", trenutni);
                                fr.setArguments(argumenti);
                            }
                            fm.beginTransaction().replace(R.id.mjestoZaOstalo, fr).addToBackStack(null).commit();
                        } else {
                            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        }
                    }

                    FragmentManager fm1 = getFragmentManager();
                    Fragment a1 = fm1.findFragmentById(R.id.mjestoZaGlumce);
                    if (!(a1 instanceof fragment_lista_zanrova)) {
                        fragment_lista_zanrova fr1 = (fragment_lista_zanrova) fm1.findFragmentById(R.layout.lista_zanrova_fragment);
                        if (fr1 == null) {
                            fr1 = new fragment_lista_zanrova();
                            if (last_index != -1) {
                                Bundle argumenti = new Bundle();
                                argumenti.putParcelable("zanrovi", trenutni);
                                fr1.setArguments(argumenti);
                            }
                            fm1.beginTransaction().replace(R.id.mjestoZaGlumce, fr1).addToBackStack(null).commit();
                        } else {
                            fm1.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        }
                    }


                }
            });

            transaction.replace(R.id.mjestoZaGlumce, fragglumci).addToBackStack(null);
            transaction.replace(R.id.mjestoZaOstalo, detalji).addToBackStack(null);
            //transaction.commit();

            transaction.commit();
        }
        Resources res = getResources();


       /*zanrovi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment_lista_zanrova zanr = new fragment_lista_zanrova();
                transaction.replace(R.id.mjestoF2, zanr) ;
                transaction.commit();
            }
        });

        glumci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment_lista_glumaca glumac = new fragment_lista_glumaca();
                transaction.replace(R.id.mjestoF2, glumac) ;
                transaction.commit();
            }
        });

        glumci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment_lista_rezisera reziser = new fragment_lista_rezisera();
                transaction.replace(R.id.mjestoF2, reziser) ;
                transaction.commit();
            }
        });
*/


/*



        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Glumac glumac = glumci.get(position);
                Intent intent=new Intent(MainActivity.this,Main2Activity.class);
                intent.putExtra("imeGl",glumac.getIme());
                intent.putExtra("prezimeGl",glumac.getPrezime());
                intent.putExtra("godinaRodjenjaGl",glumac.getGodinaRodjenja());
                intent.putExtra("mjestoRodjenjaGl",glumac.getMjestoRodjenja());
                intent.putExtra("godinaSmrtiGl",glumac.getGodinaSmrti());
                intent.putExtra("bioGl",glumac.getBiografija());
                intent.putExtra("linkGl",glumac.getLink());
                intent.putExtra("spolGl", glumac.getSpol());
                intent.putExtra("slikaGl",Integer.toString(glumac.getSlika()));
                startActivity(intent);

            }
        });
        dugmeReziser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentR=new Intent(MainActivity.this, Main3Activity.class);
                startActivity(intentR);
            }
        });

        dugmeZanrovi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentZ=new Intent(MainActivity.this, Main4Activity.class);
                startActivity(intentZ);
            }
        });
*/
    }

   /* private void setFragment(fragment_dugmad fragmentDugmad) {
        FragmentManager fm = getFragmentManager();
        if(fm.findFragmentById(R.id.mjestoF1) == null)
            fm.beginTransaction().add(R.id.mjestoF1, fragmentDugmad).commit();

    }*/

    @Override
    public void onButtonClicked​(String s) {
        koji = s;
        if (koji == "1") {
            fragment_lista_glumaca glumac = new fragment_lista_glumaca();
            Bundle argumenti = new Bundle();
            argumenti.putParcelableArrayList("Alista", glumci);
            glumac.setArguments(argumenti);
            transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.mjestoF2, glumac).addToBackStack(null);
            //getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            // fm.popBackStack(null​,FragmentManager.POP_BACK_STACK_INCLUSIVE);

            transaction.commit();
        } else if (koji == "2") {

            FragmentManager fm = getFragmentManager();
            Fragment a = fm.findFragmentById(R.id.mjestoF2);
            if (!(a instanceof fragment_lista_rezisera)) {
                fragment_lista_rezisera fr = (fragment_lista_rezisera) fm.findFragmentById(R.layout.lista_rezisera_fragment);
                if (fr == null) {
                    fr = new fragment_lista_rezisera();
                    if (last_index != -1) {
                        Bundle argumenti = new Bundle();
                        argumenti.putParcelable("reziseri", trenutni);
                        fr.setArguments(argumenti);
                    }
                    fm.beginTransaction().replace(R.id.mjestoF2, fr).addToBackStack(null).commit();
                } else {
                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
            }

        } else if (koji == "3") {
            FragmentManager fm = getFragmentManager();
            Fragment a = fm.findFragmentById(R.id.mjestoF2);
            if (!(a instanceof fragment_lista_zanrova)) {
                fragment_lista_zanrova fr = (fragment_lista_zanrova) fm.findFragmentById(R.layout.lista_zanrova_fragment);
                if (fr == null) {
                    fr = new fragment_lista_zanrova();
                    if (last_index != -1) {
                        Bundle argumenti = new Bundle();
                        argumenti.putParcelable("zanrovi", trenutni);
                        fr.setArguments(argumenti);
                    }
                    fm.beginTransaction().replace(R.id.mjestoF2, fr).addToBackStack(null).commit();
                } else {
                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
            }

        } else if (koji == "4") {
            fragment_filmovi fl = new fragment_filmovi();
            Bundle argumenti = new Bundle();
            argumenti.putStringArrayList("filmovi", film_staticki);
            fl.setArguments(argumenti);
            getFragmentManager().beginTransaction().replace(R.id.mjestoF2, fl).addToBackStack(null).commit();
        }
    }


    @Override
    public void onItemClicked(int pos) {
        trenutni = glumci.get(pos);
        last_index = pos;
        Bundle arguments = new Bundle();
        arguments.putParcelable("glumac", glumci.get(pos));
        fragment_detalji_oglumcu fd = new fragment_detalji_oglumcu();
        fd.setArguments(arguments);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            getFragmentManager().beginTransaction().replace(R.id.mjestoZaOstalo, fd).addToBackStack(null).commit();

        } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            getFragmentManager().beginTransaction().replace(R.id.mjestoF2, fd).addToBackStack(null).commit();
        }
    }

    @Override
    public void onItemClicked1(int pos) {

        fragment_detalji_ofilmu flm = new fragment_detalji_ofilmu();
        Bundle filmovi_arg = new Bundle();
        String nesto;
        nesto = film_staticki.get(pos);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {

            filmovi_arg.putString("film", nesto);
            flm.setArguments(filmovi_arg);
            getFragmentManager().beginTransaction().replace(R.id.mjestoF2, flm).addToBackStack(null).commit();

        }


    }

    /*class Adapter1 extends ArrayAdapter<String>{
        Context contekst;
        int[] slike;
        String[] imena;
        String[] prezimena;
        String[] godRodjenja;
        String[] mjestoRodjenja;
        String[] ratings;

        Adapter1(Context c, int[] s, String[] im, String[] pr, String[] godR, String[] mjR, String[] ratin)
        {
            super(c,R.layout.glumaculisti,R.id.tekstViewP,pr);
            this.contekst = c;
            this.imena = im;
            this.slike = s;
            this.prezimena = pr;
            this.mjestoRodjenja = mjR;
            this.godRodjenja = godR;
            this.ratings = ratin;
        }*/

        /*@Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            LayoutInflater inf = (LayoutInflater) contekst.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View red = inf.inflate(R.layout.glumaculisti, parent, false);
           ImageView prikazslike = (ImageView) red.findViewById(R.id.imageView);
            TextView prikazimena = (TextView) red.findViewById(R.id.tekstViewI);
            TextView prikazprezimena = (TextView) red.findViewById(R.id.tekstViewP);
            TextView prikazgodineRodjenja = (TextView) red.findViewById(R.id.tekstViewGR);
            TextView prikazmjestaRodjenja = (TextView) red.findViewById(R.id.tekstViewMR);
            TextView prikazratinga = (TextView) red.findViewById(R.id.tekstViewRating);

            return red;
        }
        */


        public void onBackPressed() {
            if (getFragmentManager().getBackStackEntryCount() != 0) {
                getFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        }

    }


