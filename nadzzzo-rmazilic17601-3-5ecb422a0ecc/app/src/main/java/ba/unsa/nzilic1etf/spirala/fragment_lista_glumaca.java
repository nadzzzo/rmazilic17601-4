package ba.unsa.nzilic1etf.spirala;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.opengl.GLU;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by User on 10.4.2017.
 */

public class fragment_lista_glumaca extends Fragment implements SearchActor.OnMuzicarSearchDone, ResiverZaRezultat.Receiver {
   public static ArrayList<Glumac> glumci = new ArrayList<>();
    public static ArrayList<Glumac> glumci_iz_baze = new ArrayList<>();
    private OnItemClick oic;
    ListView lista;
     public static AdapterListe adapter = null;

    //ResiverZaRezultat resiver;

    @Override
    public void onDone(ArrayList<Glumac> rez) {

 glumci.clear();
        for(int i = 0; i<rez.size(); i++)
        {
            glumci.add(rez.get(i));
        }
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        switch (resultCode) {

         case MyIntentService.STATUS_RUNNING:
            Toast.makeText(getActivity(), "Pretraga u toku. Molimo pričekajte!",
                    Toast.LENGTH_LONG).show();
            break;

            case MyIntentService.STATUS_FINISHED:
                glumci.clear();
                ArrayList<Glumac> rez = resultData.getParcelableArrayList("ListaGlumaca");
                for (Glumac g : rez) {
                    glumci.add(g);
                }
                adapter.notifyDataSetChanged();
                break;

         case MyIntentService.STATUS_ERROR:
            Toast.makeText(getActivity(), "Neispravan unos!",
                    Toast.LENGTH_LONG).show();
            break;
        }

        }


    public interface OnItemClick
    {
        public void onItemClicked(int pos);
    }

    @Override
    public void onActivityCreated( Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        }


        @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View view = inflater.inflate(R.layout.lista_glumaca_fragment, container, false);
            Button dgm=(Button) view.findViewById(R.id.button4);
            final EditText e = (EditText)view.findViewById(R.id.editText);

            lista = (ListView)view.findViewById(R.id.listView);


            if (getArguments().containsKey("Alista")) {

                glumci = getArguments().getParcelableArrayList("Alista");
                TextView emptyText = (TextView)view.findViewById(R.id.textView2);

                adapter = new AdapterListe(getActivity(), glumci, this.getResources());
                lista.setAdapter(adapter);

                try {
                    oic = (OnItemClick) getActivity();
                } catch (ClassCastException ed) {
                    throw new ClassCastException(getActivity().toString() + "Treba implementirati OnItemClick");
                }
                lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        oic.onItemClicked(position);
                    }
                });

                dgm.setOnClickListener(new View.OnClickListener()

                                       {
                                           @Override
                                           public void onClick (View v){
                                               String ime_koje_trazim = e.getText().toString();
                                                    if(ime_koje_trazim.contains("actor:"))
                                                    {
                                                            CitajIzBazeX(ime_koje_trazim);


                                                    }
                                                    else if(ime_koje_trazim.contains("director:"))
                                                    {
                                                        CitajIzBazeReziseri(ime_koje_trazim);
                                                    }
                                                    else {

                                                        Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), MyIntentService.class);
                                                        ResiverZaRezultat resiver = new ResiverZaRezultat(new Handler());

                                                        resiver.setReceiver(fragment_lista_glumaca.this);

                                                        intent.putExtra("ime", ime_koje_trazim);
                                                        intent.putExtra("resiver", resiver);
                                                        getActivity().startService(intent);
                                                    }
                                           }
                                       }
                );

            }

            return view;
        }//director:

        public void CitajIzBazeReziseri(String q)
        {
            glumci.clear();
            String koga = q.substring(9);
            String[] koloneRezultat = new String[]{
                    DataBaseAdapter.KEY_personID,
                    DataBaseAdapter.KEY_NAME,
                    DataBaseAdapter.KEY_LASTNAME,
                    DataBaseAdapter.KEY_BIRTHDAY,
                    DataBaseAdapter.KEY_DEATHDAY,
                    DataBaseAdapter.KEY_BIOGRAPHY,
                    DataBaseAdapter.KEY_BIRTHPLACE,
                    DataBaseAdapter.KEY_GENDER,
                    DataBaseAdapter.KEY_IMDB ,
                    DataBaseAdapter.KEY_PICTURE,
                    DataBaseAdapter.KEY_RATING,
                    DataBaseAdapter.KEY_REZISERI,
                    DataBaseAdapter.KEY_ZANROVI


            };

            String where = DataBaseAdapter.KEY_REZISERI + " LIKE '%" + koga + "%'" ;
            DataBaseAdapter adpt = new DataBaseAdapter(getActivity());


            SQLiteDatabase database = adpt.getReadableDatabase();
            Cursor cursor = database.query(adpt.DATABASE_TABLE1,koloneRezultat,where,null,null,null,null);
            //cursor.moveToFirst();


            while (cursor.moveToNext()) {
                String ko = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_NAME));
                String spol = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_GENDER));
                String prezime = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_LASTNAME));
                String mjesto = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_BIRTHPLACE));
                String link = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_IMDB));
                String biografija = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_BIOGRAPHY));
                String rank = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_RATING));
                String slika = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_PICTURE));
                String rod = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_BIRTHDAY));
                String umr = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_DEATHDAY));
                //ArrayList<String> zanr = cursor.getColumnIndexOrThrow(DataBaseAdapter.KEY_GENDER);
                Integer gid = cursor.getInt(cursor.getColumnIndex(DataBaseAdapter.KEY_personID));
                String rezz = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_REZISERI));
                String znr =  cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_ZANROVI));


                ArrayList<Reziser> reziseri= new ArrayList<>();
                String[] parts = rezz.split(",");
                int len = parts.length;
                if(len == 1)
                {  String part1 = parts[0];
                    Reziser r1 = new Reziser(part1, "");
                    reziseri.add(r1);
                }
                else if (len == 2)
                {
                    String part1 = parts[0];
                    String part2 = parts[1];
                    Reziser r1 = new Reziser(part1, "");
                    Reziser r2 = new Reziser(part2, "");
                    reziseri.add(r1);
                    reziseri.add(r2);
                }
                else if(len == 3)
                {
                    String part1 = parts[0];
                    if(part1.contains("null")) part1=part1.substring(4);
                    String part2 = parts[1];
                    String part3 = parts[2];
                    Reziser r1 = new Reziser(part1, "");
                    Reziser r2 = new Reziser(part2, "");
                    Reziser r3 = new Reziser(part3, "");
                    reziseri.add(r1);
                    reziseri.add(r2);
                    reziseri.add(r3);
                }
                else if(len == 4)
                {
                    String part1 = parts[0];
                    String part2 = parts[1];
                    String part3 = parts[2];
                    String part4 = parts[3];
                    Reziser r1 = new Reziser(part1, "");
                    Reziser r2 = new Reziser(part2, "");
                    Reziser r3 = new Reziser(part3, "");
                    Reziser r4 = new Reziser(part4, "");
                    reziseri.add(r1);
                    reziseri.add(r2);
                    reziseri.add(r3);
                    reziseri.add(r4);
                }
                else if(len == 5)
                {
                    String part1 = parts[0];
                    String part2 = parts[1];
                    String part3 = parts[2];
                    String part4 = parts[3];
                    String part5 =parts[4];
                    Reziser r1 = new Reziser(part1, "");
                    Reziser r2 = new Reziser(part2, "");
                    Reziser r3 = new Reziser(part3, "");
                    Reziser r4 = new Reziser(part4, "");
                    Reziser r5 = new Reziser(part5, "");
                    reziseri.add(r1);
                    reziseri.add(r2);
                    reziseri.add(r3);
                    reziseri.add(r4);
                    reziseri.add(r5);
                }
                else if(len == 6)
                {
                    String part1 = parts[0];
                    String part2 = parts[1];
                    String part3 = parts[2];
                    String part4 = parts[3];
                    String part5 =parts[4];
                    String part6 = parts[5];
                    Reziser r1 = new Reziser(part1, "");
                    Reziser r2 = new Reziser(part2, "");
                    Reziser r3 = new Reziser(part3, "");
                    Reziser r4 = new Reziser(part4, "");
                    Reziser r5 = new Reziser(part5, "");
                    Reziser r6 = new Reziser(part6, "");
                    reziseri.add(r1);
                    reziseri.add(r2);
                    reziseri.add(r3);
                    reziseri.add(r4);
                    reziseri.add(r5);
                    reziseri.add(r6);
                }
                else if(len == 7)
                {
                    String part1 = parts[0];
                    String part2 = parts[1];
                    String part3 = parts[2];
                    String part4 = parts[3];
                    String part5 =parts[4];
                    String part6 = parts[5];
                    String part7 = parts[6];

                    Reziser r1 = new Reziser(part1, "");
                    Reziser r2 = new Reziser(part2, "");
                    Reziser r3 = new Reziser(part3, "");
                    Reziser r4 = new Reziser(part4, "");
                    Reziser r5 = new Reziser(part5, "");
                    Reziser r6 = new Reziser(part6, "");
                    Reziser r7 = new Reziser(part7, "");

                    reziseri.add(r1);
                    reziseri.add(r2);
                    reziseri.add(r3);
                    reziseri.add(r4);
                    reziseri.add(r5);
                    reziseri.add(r6);
                    reziseri.add(r7);

                }


                ArrayList<Zanr> zanrovi= new ArrayList<>();

                String[] parts1 = znr.split(",");
                int leng = parts1.length;
                if(leng == 1)
                {


                    String part11 = parts1[0];
                    if(part11.contains("null")) part11=part11.substring(4);
                    Zanr z1 = new Zanr(R.drawable.action, part11);
                    zanrovi.add(z1);
                }
                else if(leng == 2)
                {
                    String part11 = parts1[0];
                    if(part11.contains("null")) part11=part11.substring(4);
                    String part21 = parts1[1];
                    Zanr z1 = new Zanr(R.drawable.action, part11);
                    Zanr z2 = new Zanr(R.drawable.action, part21);
                    zanrovi.add(z1);
                    zanrovi.add(z2);

                }
                else if(leng == 3)
                {
                    String part11 = parts1[0];
                    if(part11.contains("null")) part11=part11.substring(4);
                    String part21 = parts1[1];
                    String part31 = parts1[2];
                    Zanr z1 = new Zanr(R.drawable.action, part11);
                    Zanr z2 = new Zanr(R.drawable.action, part21);
                    Zanr z3 = new Zanr(R.drawable.action, part31);
                    zanrovi.add(z1);
                    zanrovi.add(z2);
                    zanrovi.add(z3);

                }
                else if(leng == 4)
                {
                    String part11 = parts1[0];
                    if(part11.contains("null")) part11=part11.substring(4);
                    String part21 = parts1[1];
                    String part31 = parts1[2];
                    String part41 = parts1[3];

                    Zanr z1 = new Zanr(R.drawable.action, part11);
                    Zanr z2 = new Zanr(R.drawable.action, part21);
                    Zanr z3 = new Zanr(R.drawable.action, part31);
                    Zanr z4 = new Zanr(R.drawable.action, part41);

                    zanrovi.add(z1);
                    zanrovi.add(z2);
                    zanrovi.add(z3);
                    zanrovi.add(z4);
                }
                else if(leng == 5)
                {
                    String part11 = parts1[0];
                    if(part11.contains("null")) part11=part11.substring(4);
                    String part21 = parts1[1];
                    String part31 = parts1[2];
                    String part41 = parts1[3];
                    String part51 =parts1[4];

                    Zanr z1 = new Zanr(R.drawable.action, part11);
                    Zanr z2 = new Zanr(R.drawable.action, part21);
                    Zanr z3 = new Zanr(R.drawable.action, part31);
                    Zanr z4 = new Zanr(R.drawable.action, part41);
                    Zanr z5 = new Zanr(R.drawable.action, part51);

                    zanrovi.add(z1);
                    zanrovi.add(z2);
                    zanrovi.add(z3);
                    zanrovi.add(z4);
                    zanrovi.add(z5);
                }
                else if(leng == 6)
                {
                    String part11 = parts1[0];
                    if(part11.contains("null")) part11=part11.substring(4);
                    String part21 = parts1[1];
                    String part31 = parts1[2];
                    String part41 = parts1[3];
                    String part51 =parts1[4];
                    String part61 = parts1[5];

                    Zanr z1 = new Zanr(R.drawable.action, part11);
                    Zanr z2 = new Zanr(R.drawable.action, part21);
                    Zanr z3 = new Zanr(R.drawable.action, part31);
                    Zanr z4 = new Zanr(R.drawable.action, part41);
                    Zanr z5 = new Zanr(R.drawable.action, part51);
                    Zanr z6 = new Zanr(R.drawable.action, part61);
                    zanrovi.add(z1);
                    zanrovi.add(z2);
                    zanrovi.add(z3);
                    zanrovi.add(z4);
                    zanrovi.add(z5);
                    zanrovi.add(z6);
                }
                else if(leng == 7)
                {
                    String part11 = parts1[0];
                    if(part11.contains("null")) part11=part11.substring(4);
                    String part21 = parts1[1];
                    String part31 = parts1[2];
                    String part41 = parts1[3];
                    String part51 =parts1[4];
                    String part61 = parts1[5];
                    String part71 = parts1[6];

                    Zanr z1 = new Zanr(R.drawable.action, part11);
                    Zanr z2 = new Zanr(R.drawable.action, part21);
                    Zanr z3 = new Zanr(R.drawable.action, part31);
                    Zanr z4 = new Zanr(R.drawable.action, part41);
                    Zanr z5 = new Zanr(R.drawable.action, part51);
                    Zanr z6 = new Zanr(R.drawable.action, part61);
                    Zanr z7 = new Zanr(R.drawable.action, part71);


                    zanrovi.add(z1);
                    zanrovi.add(z2);
                    zanrovi.add(z3);
                    zanrovi.add(z4);
                    zanrovi.add(z5);
                    zanrovi.add(z6);
                    zanrovi.add(z7);
                }



                Glumac gl = new Glumac(slika,ko,prezime,rod,mjesto,rank,spol,umr,biografija,link);
                gl.dodajRezisere(reziseri);
                gl.dodajZanrove(zanrovi);
                glumci.add(gl);
            }

            cursor.close();

            adapter = new AdapterListe(getActivity(), glumci, this.getResources());
            lista.setAdapter(adapter);


        }


    public void CitajIzBazeX(String query)
    {
        glumci.clear();
        String koga = query.substring(6);
        String[] koloneRezultat = new String[]{
                DataBaseAdapter.KEY_personID,
                DataBaseAdapter.KEY_NAME,
                DataBaseAdapter.KEY_LASTNAME,
                DataBaseAdapter.KEY_BIRTHDAY,
                DataBaseAdapter.KEY_DEATHDAY,
                DataBaseAdapter.KEY_BIOGRAPHY,
                DataBaseAdapter.KEY_BIRTHPLACE,
                DataBaseAdapter.KEY_GENDER,
                DataBaseAdapter.KEY_IMDB ,
                DataBaseAdapter.KEY_PICTURE,
                DataBaseAdapter.KEY_RATING,
                DataBaseAdapter.KEY_REZISERI,
                DataBaseAdapter.KEY_ZANROVI


        };
        String where = DataBaseAdapter.KEY_NAME + " LIKE '%" + koga + "%'" ;
        DataBaseAdapter adpt = new DataBaseAdapter(getActivity());


        SQLiteDatabase database = adpt.getReadableDatabase();
        Cursor cursor = database.query(adpt.DATABASE_TABLE1,koloneRezultat,where,null,null,null,null);
        //cursor.moveToFirst();


        while (cursor.moveToNext()) {
            String ko = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_NAME));
            String spol = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_GENDER));
            String prezime = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_LASTNAME));
            String mjesto = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_BIRTHPLACE));
            String link = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_IMDB));
            String biografija = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_BIOGRAPHY));
            String rank = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_RATING));
            String slika = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_PICTURE));
            String rod = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_BIRTHDAY));
            String umr = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_DEATHDAY));
            //ArrayList<String> zanr = cursor.getColumnIndexOrThrow(DataBaseAdapter.KEY_GENDER);
            Integer gid = cursor.getInt(cursor.getColumnIndex(DataBaseAdapter.KEY_personID));
            String rezz = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_REZISERI));
            String znr =  cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_ZANROVI));


            ArrayList<Reziser> reziseri= new ArrayList<>();
            String[] parts = rezz.split(",");
            int len = parts.length;
            if(len == 1)
            {  String part1 = parts[0];
                Reziser r1 = new Reziser(part1, "");
                reziseri.add(r1);
            }
            else if (len == 2)
            {
                String part1 = parts[0];
                String part2 = parts[1];
                Reziser r1 = new Reziser(part1, "");
                Reziser r2 = new Reziser(part2, "");
                reziseri.add(r1);
                reziseri.add(r2);
            }
            else if(len == 3)
            {
                String part1 = parts[0];
                if(part1.contains("null")) part1=part1.substring(4);
                String part2 = parts[1];
                String part3 = parts[2];
                Reziser r1 = new Reziser(part1, "");
                Reziser r2 = new Reziser(part2, "");
                Reziser r3 = new Reziser(part3, "");
                reziseri.add(r1);
                reziseri.add(r2);
                reziseri.add(r3);
            }
            else if(len == 4)
            {
                String part1 = parts[0];
                String part2 = parts[1];
                String part3 = parts[2];
                String part4 = parts[3];
                Reziser r1 = new Reziser(part1, "");
                Reziser r2 = new Reziser(part2, "");
                Reziser r3 = new Reziser(part3, "");
                Reziser r4 = new Reziser(part4, "");
                reziseri.add(r1);
                reziseri.add(r2);
                reziseri.add(r3);
                reziseri.add(r4);
            }
            else if(len == 5)
            {
                String part1 = parts[0];
                String part2 = parts[1];
                String part3 = parts[2];
                String part4 = parts[3];
                String part5 =parts[4];
                Reziser r1 = new Reziser(part1, "");
                Reziser r2 = new Reziser(part2, "");
                Reziser r3 = new Reziser(part3, "");
                Reziser r4 = new Reziser(part4, "");
                Reziser r5 = new Reziser(part5, "");
                reziseri.add(r1);
                reziseri.add(r2);
                reziseri.add(r3);
                reziseri.add(r4);
                reziseri.add(r5);
            }
            else if(len == 6)
            {
                String part1 = parts[0];
                String part2 = parts[1];
                String part3 = parts[2];
                String part4 = parts[3];
                String part5 =parts[4];
                String part6 = parts[5];
                Reziser r1 = new Reziser(part1, "");
                Reziser r2 = new Reziser(part2, "");
                Reziser r3 = new Reziser(part3, "");
                Reziser r4 = new Reziser(part4, "");
                Reziser r5 = new Reziser(part5, "");
                Reziser r6 = new Reziser(part6, "");
                reziseri.add(r1);
                reziseri.add(r2);
                reziseri.add(r3);
                reziseri.add(r4);
                reziseri.add(r5);
                reziseri.add(r6);
            }
            else if(len == 7)
            {
                String part1 = parts[0];
                String part2 = parts[1];
                String part3 = parts[2];
                String part4 = parts[3];
                String part5 =parts[4];
                String part6 = parts[5];
                String part7 = parts[6];

                Reziser r1 = new Reziser(part1, "");
                Reziser r2 = new Reziser(part2, "");
                Reziser r3 = new Reziser(part3, "");
                Reziser r4 = new Reziser(part4, "");
                Reziser r5 = new Reziser(part5, "");
                Reziser r6 = new Reziser(part6, "");
                Reziser r7 = new Reziser(part7, "");

                reziseri.add(r1);
                reziseri.add(r2);
                reziseri.add(r3);
                reziseri.add(r4);
                reziseri.add(r5);
                reziseri.add(r6);
                reziseri.add(r7);

            }


            ArrayList<Zanr> zanrovi= new ArrayList<>();

            String[] parts1 = znr.split(",");
            int leng = parts1.length;
            if(leng == 1)
            {


                String part11 = parts1[0];
                if(part11.contains("null")) part11=part11.substring(4);
                Zanr z1 = new Zanr(R.drawable.action, part11);
                zanrovi.add(z1);
            }
            else if(leng == 2)
            {
                String part11 = parts1[0];
                if(part11.contains("null")) part11=part11.substring(4);
                String part21 = parts1[1];
                Zanr z1 = new Zanr(R.drawable.action, part11);
                Zanr z2 = new Zanr(R.drawable.action, part21);
                zanrovi.add(z1);
                zanrovi.add(z2);

            }
            else if(leng == 3)
            {
                String part11 = parts1[0];
                if(part11.contains("null")) part11=part11.substring(4);
                String part21 = parts1[1];
                String part31 = parts1[2];
                Zanr z1 = new Zanr(R.drawable.action, part11);
                Zanr z2 = new Zanr(R.drawable.action, part21);
                Zanr z3 = new Zanr(R.drawable.action, part31);
                zanrovi.add(z1);
                zanrovi.add(z2);
                zanrovi.add(z3);

            }
            else if(leng == 4)
            {
                String part11 = parts1[0];
                if(part11.contains("null")) part11=part11.substring(4);
                String part21 = parts1[1];
                String part31 = parts1[2];
                String part41 = parts1[3];

                Zanr z1 = new Zanr(R.drawable.action, part11);
                Zanr z2 = new Zanr(R.drawable.action, part21);
                Zanr z3 = new Zanr(R.drawable.action, part31);
                Zanr z4 = new Zanr(R.drawable.action, part41);

                zanrovi.add(z1);
                zanrovi.add(z2);
                zanrovi.add(z3);
                zanrovi.add(z4);
            }
            else if(leng == 5)
            {
                String part11 = parts1[0];
                if(part11.contains("null")) part11=part11.substring(4);
                String part21 = parts1[1];
                String part31 = parts1[2];
                String part41 = parts1[3];
                String part51 =parts1[4];

                Zanr z1 = new Zanr(R.drawable.action, part11);
                Zanr z2 = new Zanr(R.drawable.action, part21);
                Zanr z3 = new Zanr(R.drawable.action, part31);
                Zanr z4 = new Zanr(R.drawable.action, part41);
                Zanr z5 = new Zanr(R.drawable.action, part51);

                zanrovi.add(z1);
                zanrovi.add(z2);
                zanrovi.add(z3);
                zanrovi.add(z4);
                zanrovi.add(z5);
            }
            else if(leng == 6)
            {
                String part11 = parts1[0];
                if(part11.contains("null")) part11=part11.substring(4);
                String part21 = parts1[1];
                String part31 = parts1[2];
                String part41 = parts1[3];
                String part51 =parts1[4];
                String part61 = parts1[5];

                Zanr z1 = new Zanr(R.drawable.action, part11);
                Zanr z2 = new Zanr(R.drawable.action, part21);
                Zanr z3 = new Zanr(R.drawable.action, part31);
                Zanr z4 = new Zanr(R.drawable.action, part41);
                Zanr z5 = new Zanr(R.drawable.action, part51);
                Zanr z6 = new Zanr(R.drawable.action, part61);
                zanrovi.add(z1);
                zanrovi.add(z2);
                zanrovi.add(z3);
                zanrovi.add(z4);
                zanrovi.add(z5);
                zanrovi.add(z6);
            }
            else if(leng == 7)
            {
                String part11 = parts1[0];
                if(part11.contains("null")) part11=part11.substring(4);
                String part21 = parts1[1];
                String part31 = parts1[2];
                String part41 = parts1[3];
                String part51 =parts1[4];
                String part61 = parts1[5];
                String part71 = parts1[6];

                Zanr z1 = new Zanr(R.drawable.action, part11);
                Zanr z2 = new Zanr(R.drawable.action, part21);
                Zanr z3 = new Zanr(R.drawable.action, part31);
                Zanr z4 = new Zanr(R.drawable.action, part41);
                Zanr z5 = new Zanr(R.drawable.action, part51);
                Zanr z6 = new Zanr(R.drawable.action, part61);
                Zanr z7 = new Zanr(R.drawable.action, part71);


                zanrovi.add(z1);
                zanrovi.add(z2);
                zanrovi.add(z3);
                zanrovi.add(z4);
                zanrovi.add(z5);
                zanrovi.add(z6);
                zanrovi.add(z7);
            }



            Glumac gl = new Glumac(slika,ko, prezime,rod,mjesto,rank,spol,umr,biografija,link);
            gl.dodajRezisere(reziseri);
            gl.dodajZanrove(zanrovi);
            glumci.add(gl);
        }

        cursor.close();

        adapter = new AdapterListe(getActivity(), glumci, this.getResources());
        lista.setAdapter(adapter);



    }







    class Adapter1 extends ArrayAdapter<String> {
        Context contekst;
        int[] slike;
        String[] imena;
        String[] prezimena;
        String[] godRodjenja;
        String[] mjestoRodjenja;
        String[] ratings;

        Adapter1(Context c, int[] s, String[] im, String[] pr, String[] godR, String[] mjR, String[] ratin) {
            super(c, R.layout.glumaculisti, R.id.tekstViewP, pr);
            this.contekst = c;
            this.imena = im;
            this.slike = s;
            this.prezimena = pr;
            this.mjestoRodjenja = mjR;
            this.godRodjenja = godR;
            this.ratings = ratin;
        }

    }

}
