package ba.unsa.nzilic1etf.spirala;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by User on 28.3.2017.
 */

public class AdapterListe extends BaseAdapter {

    private Activity aktivnost;
    private ArrayList podaci;
    private static LayoutInflater inflater = null;
    public Resources res;
    Glumac privVrijednosti = null;
    int i = 0;

    public AdapterListe(Activity a, ArrayList e, Resources res) {
        this.aktivnost = a;
        this.podaci = e;
        this.res = res;
        inflater = (LayoutInflater) aktivnost.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        if (podaci.size() <= 0) return 1;
        return podaci.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class PrikazReda {
        public ImageView slika;
        public TextView tekstime;
        public TextView tekstprezime;
        public TextView tekstgodinaRodjenja;
        public TextView tekstmjestoRodjenja;
        public TextView tekstRating;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View pogled = convertView;
        PrikazReda prikaz;

        if (convertView == null) {
            pogled = inflater.inflate(R.layout.glumaculisti, null);

            prikaz = new PrikazReda();
            prikaz.slika = (ImageView) pogled.findViewById(R.id.imageView);
            prikaz.tekstime = (TextView) pogled.findViewById(R.id.tekstViewI);
            prikaz.tekstprezime = (TextView) pogled.findViewById(R.id.tekstViewP);
            prikaz.tekstgodinaRodjenja = (TextView) pogled.findViewById(R.id.tekstViewGR);
            prikaz.tekstmjestoRodjenja = (TextView) pogled.findViewById(R.id.tekstViewMR);
            prikaz.tekstRating = (TextView) pogled.findViewById(R.id.tekstViewRating);

            pogled.setTag(prikaz);

        } else prikaz = (PrikazReda) pogled.getTag();

        if (podaci.size() <= 0) {
            //prikaz.tekstime.setText("Nema podataka");

        } else {
            privVrijednosti = null;
            privVrijednosti = (Glumac) podaci.get(position);



            Picasso.with(parent.getContext()).load("https://image.tmdb.org/t/p/original/"+privVrijednosti.getSlika()).into(prikaz.slika);
            prikaz.tekstime.setText(privVrijednosti.getIme());
            prikaz.tekstprezime.setText(privVrijednosti.getPrezime());
            prikaz.tekstgodinaRodjenja.setText(privVrijednosti.getGodinaRodjenja());
            prikaz.tekstmjestoRodjenja.setText(privVrijednosti.getMjestoRodjenja());
            prikaz.tekstRating.setText(privVrijednosti.getRating());

        }
        return pogled;
    }
}