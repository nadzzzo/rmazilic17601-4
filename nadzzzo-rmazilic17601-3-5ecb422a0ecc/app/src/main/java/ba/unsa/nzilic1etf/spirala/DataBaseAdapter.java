package ba.unsa.nzilic1etf.spirala;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;

import javax.xml.datatype.Duration;

/**
 * Created by User on 9.6.2017.
 */

public class DataBaseAdapter extends SQLiteOpenHelper {
    public static final String DATABASE = "17601DataBase";
    public static final String KEY_personID = "_id";
    public static final String KEY_NAME = "Ime";
    public static final String KEY_LASTNAME = "Prezime";
    public static final String KEY_BIRTHDAY = "Godina_Rodjenja";
    public static final String KEY_DEATHDAY = "Godina_Smrti";
    public static final String KEY_BIOGRAPHY = "Biografija";
    public static final String KEY_BIRTHPLACE = "Mjesto_Rodjenja";
    public static final String KEY_GENDER = "Pol";
    public static final String KEY_IMDB = "IMDB";
    public static final String KEY_PICTURE = "Slika";
    public static final String KEY_RATING = "Rejting";
    public static final String KEY_genreID = "_zanrid";
    public static final String KEY_genreNAME = "Ime_zanra";
    public static final String KEY_directorID = "_reziserid";
    public static final String KEY_directorNAME = "Ime_rezisera";


    public static final String KEY_ZANROVI = "_Zanrovi";
    public static final String KEY_REZISERI = "_Reziseri";


    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_TABLE1 = "Glumci";
    public static final String DATABASE_TABLE2 = "Zanrovi";
    public static final String DATABASE_TABLE3 = "Reziseri";


    private static final String DATABASE_CREATE1 = "create table " +
            DATABASE_TABLE1 + " (" + KEY_personID +
            " integer primary key autoincrement, " +
            KEY_NAME + " text not null, " + KEY_LASTNAME + " text not null, " + KEY_BIRTHDAY + " text not null, " +
            KEY_DEATHDAY + " text not null, " + KEY_BIOGRAPHY + "  null, " + KEY_BIRTHPLACE + " text not null, " +
            KEY_GENDER + " text not null, " + KEY_IMDB + " text not null, " +
            KEY_PICTURE + " text not null, " + KEY_RATING + " text not null, " + KEY_REZISERI + " text not null, " + KEY_ZANROVI
            + " text not null );";


    private static final String DATABASE_CREATE2 = "create table " +
            DATABASE_TABLE2 + " (" + KEY_genreID +
            " integer, " +
            KEY_genreNAME + " text not null);";

    private static final String DATABASE_CREATE3 = "create table " +
            DATABASE_TABLE3 + " (" + KEY_directorID +
            " integer, " +
            KEY_directorNAME + " text not null);";


    Context context;
    public static SQLiteDatabase db;
    GlumciDataBOpenHelper dataBOpenHelper; //********************
    ArrayList<Zanr> zanrovi = new ArrayList<>();
    // ArrayList<String> reziseri= new ArrayList<>();

    public DataBaseAdapter(Context context) {
        super(context, DATABASE, null, DATABASE_VERSION);

        this.context = context;
        dataBOpenHelper = new GlumciDataBOpenHelper(context);
    }



 /*   public DataBaseAdapter (Context ctx)
    {

        this.context=ctx;
        dataBOpenHelper = new GlumciDataBOpenHelper(context);

    }
    */


    public static class GlumciDataBOpenHelper extends SQLiteOpenHelper {

        public GlumciDataBOpenHelper(Context context) {
            super(context, DATABASE, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_CREATE1);
            db.execSQL(DATABASE_CREATE2);
            db.execSQL(DATABASE_CREATE3);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            StringBuffer bf = new StringBuffer();

            db.execSQL("DROP TABLE IF IT EXISTS" + DATABASE_TABLE1);
            db.execSQL("DROP TABLE IF IT EXISTS" + DATABASE_TABLE2);
            db.execSQL("DROP TABLE IF IT EXISTS" + DATABASE_TABLE3);
            bf.append("Uspjesan upgrade");
            onCreate(db);
        }
    }

    public DataBaseAdapter open() throws SQLException {
        db = dataBOpenHelper.getWritableDatabase();
        return this;
    }

    public void insertGlumac(String imeiprezime,
                             String godinarodjenja,
                             String godinasmrti,
                             String mjestrodjenja,
                             String spol,
                             String slika,
                             String biografija,
                             String web,
                             String rejting,
                             String reziseri,
                             String znr )
    {

        int idglumca = 0;
        String imeGlumca = imeiprezime.substring(0, imeiprezime.indexOf(" "));
        String prezimeGlumca = imeiprezime.substring(imeiprezime.indexOf(" ")+1);

        Cursor glumci = getAllActors();
        Cursor reziserii = getAllDirectors();
        Cursor zanrovii = getAllGenres();


        ContentValues cv = new ContentValues();
        cv.put("ime", imeGlumca);
        cv.put("Prezime", prezimeGlumca);
        cv.put("godina_rodjenja", godinarodjenja);
        cv.put("mjesto_rodjenja", mjestrodjenja);
        cv.put("godina_smrti", godinasmrti);
        cv.put("pol", spol);
        cv.put("slika", slika);
        cv.put("biografija", biografija);
        cv.put("imdb", web);
        cv.put("rejting", rejting);
        cv.put("_reziseri", reziseri);
        cv.put("_zanrovi", znr);


        boolean imaga = false;
        if (glumci.moveToFirst()) {
            do {
                if (imeGlumca.equals(glumci.getString(1)) && prezimeGlumca.equals(glumci.getString(2))) {
                    Toast.makeText(context, "Dupli glumac", Toast.LENGTH_LONG).show();
                    idglumca = glumci.getInt(0);
                    imaga = true;
                    break;
                }
            } while (glumci.moveToNext());

        }
        if (!imaga) {

            for (Zanr z : zanrovi) {
                if (zanrovii.moveToFirst()) {
                    do {
                        if (zanrovii.getString(0).equals(z.getNaziv())) {
                            Toast.makeText(context, "Dupli žanr", Toast.LENGTH_LONG).show();

                            imaga = true;
                        }

                    } while (zanrovii.moveToNext());

                }
                if (!imaga) {
                    db.execSQL("INSERT INTO " +
                            DATABASE_TABLE2 + " " +
                            "(" + KEY_genreNAME +
                            " ," + KEY_genreID + "" +
                            ") VALUES ('" + z.getNaziv() + "' , " + idglumca + ");");

                }
            }
            ContentValues rv = new ContentValues();
           /* for (String s : reziseri) {
                if (reziserii.moveToFirst()) {
                    do {
                        if (reziserii.getString(0).equals(s)) {
                            Toast.makeText(context, "Dupli režiser",Toast.LENGTH_LONG).show();

                            imaga = true;
                        }
                    } while (reziserii.moveToNext());

                }
                if (!imaga)
                {
                    db.execSQL("INSERT INTO " + DATABASE_TABLE3 + " (" + KEY_directorNAME + " ," + KEY_directorID + ") VALUES ('" + s + "' , " + idglumca + ");");

                }
            }*/

            Toast.makeText(context, "Glumac dodan", Toast.LENGTH_LONG).show();
            db.insert(DATABASE_TABLE1, null, cv);
        }

    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    public void close() {
        dataBOpenHelper.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public boolean deleteActor(long rowId) {

        return db.delete(DATABASE_TABLE1, KEY_personID + "=" + rowId, null) > 0;
    }

    /*public boolean deleteglumcabyname(String ime) {
        String imegl = ime.substring(0, ime.indexOf(" "));

        return db.delete(DATABASE_TABLE1, KEY_NAME + "='" + imegl + "'", null) > 0;
        //Toast.makeText(context, "Glumac dodan",Toast.LENGTH_LONG).show();
    }
*/public boolean deleteglumcabyname(String ime)
    {
        String imegl =ime.substring(0, ime.indexOf(" "));
        int razmak = ime.indexOf(" ")+1;
        String prez= ime.substring(razmak);
        return db.delete(DATABASE_TABLE1, KEY_NAME+ "='" + imegl + "'" +" AND " +KEY_LASTNAME + "='" +prez+ "'" , null)>0;
    }

    public Cursor getAllActors() {
        return db.query(DATABASE_TABLE1,
                new String[]
                        {
                                KEY_personID,
                                KEY_NAME,
                                KEY_LASTNAME,
                                KEY_BIRTHDAY,
                                KEY_DEATHDAY,
                                KEY_BIRTHPLACE,
                                KEY_BIOGRAPHY,
                                KEY_IMDB,
                                KEY_PICTURE,
                                KEY_RATING,
                                KEY_REZISERI,
                                KEY_ZANROVI, null, null}, null, null, null, null, null, null);
    }

    public Cursor getAllGenres() {
        return db.query(DATABASE_TABLE2,
                new String[]{
                        KEY_genreID,
                        KEY_genreNAME}, null, null, null, null, null, null);
    }

    public Cursor getAllDirectors() {
        return db.query(DATABASE_TABLE3,
                new String[]{
                        KEY_directorID,
                        KEY_directorNAME}, null, null, null, null, null, null);
    }
}

