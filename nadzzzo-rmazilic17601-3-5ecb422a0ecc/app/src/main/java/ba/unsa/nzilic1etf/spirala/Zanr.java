package ba.unsa.nzilic1etf.spirala;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by User on 28.3.2017.
 */

public class Zanr implements Parcelable {
    public int getIkona() {
        return ikona;
    }

    public void setIkona(int ikona) {
        this.ikona = ikona;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    private int ikona;
    private String naziv;

    public Zanr(int i, String n){
        this.ikona=i;
        this.naziv=n;
    }
    public static final Creator<Zanr> CREATOR = new Creator<Zanr>() {
        @Override
        public Zanr createFromParcel(Parcel in)
        {
            return new Zanr(in);
        }

        @Override
        public Zanr[] newArray(int size) {
            return new Zanr[size];
        }

    };
    protected  Zanr(Parcel in) {
        naziv = in.readString();
        ikona = in.readInt();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ikona);
        dest.writeString(naziv);

    }
}
