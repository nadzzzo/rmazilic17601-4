package ba.unsa.nzilic1etf.spirala;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by User on 28.3.2017.
 */

    public class Glumac implements Parcelable {

    public Glumac() { }

        public String getSlika() {
            return slika;
        }



        public void setSlika(String slika) {
            this.slika = slika;
        }

        public String getIme() {
            return ime;
        }

        public void setIme(String ime) {
            this.ime = ime;
        }

        public String getPrezime() {
            return prezime;
        }

        public void setPrezime(String prezime) {
            this.prezime = prezime;
        }

        public String getGodinaRodjenja() {
            return godinaRodjenja;
        }

        public void setGodinaRodjenja(String godinaRodjenja) {
            this.godinaRodjenja = godinaRodjenja;
        }

        public String getMjestoRodjenja() {
            return mjestoRodjenja;
        }

        public void setMjestoRodjenja(String mjestoRodjenja) {
            this.mjestoRodjenja = mjestoRodjenja;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getSpol() {
            return spol;
        }

        public void setSpol(String spol) {
            this.spol = spol;
        }

        public String getGodinaSmrti() {
            return godinaSmrti;
        }

        public void setGodinaSmrti(String godinaSmrti) {
            this.godinaSmrti = godinaSmrti;
        }

        public String getBiografija() {
            return biografija;
        }

        public void setBiografija(String biografija) {
            this.biografija = biografija;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public void dodajRezisere(ArrayList<Reziser> r) { this.reziseri.addAll(r);}

        public void dodajZanrove(ArrayList<Zanr> r) { this.zanrovi.addAll(r);}

        public ArrayList<String> getReziseri() {
            ArrayList<String> l = new ArrayList<>();
            for(int i =0; i<reziseri.size(); i++)
            {
                String ime = reziseri.get(i).getIme() + " "+reziseri.get(i).getPrezime();
                l.add(ime);

            }
            return l; }
    public ArrayList<Zanr> getZanrovi() { return zanrovi; }



    private String slika;
        private String ime;
        private String prezime;
        private String godinaRodjenja;
        private String mjestoRodjenja;
        private String rating;
        private String spol;
        private String godinaSmrti;
        private String biografija;
        private String link;
        private ArrayList<Reziser> reziseri;
        private ArrayList<Zanr> zanrovi;


        public Glumac(String slika, String ime,String prezime, String godinaRodj, String mjestoRodj, String rating, String spol, String godinaSmr, String bio, String link)
        {
            this.slika = slika;
            this.ime = ime;
            this.prezime = prezime;
            this.godinaRodjenja = godinaRodj;
            this.godinaSmrti = godinaSmr;
            this.biografija = bio;
            this.spol  =spol;
            this.rating = rating;
            this.mjestoRodjenja = mjestoRodj;
            this.link = link;
            reziseri = new ArrayList<>();
            zanrovi = new ArrayList<>();
        }

        public static final Creator<Glumac> CREATOR = new Creator<Glumac>() {
            @Override
            public Glumac createFromParcel(Parcel in)
            {
                return new Glumac(in);
            }

            @Override
            public Glumac[] newArray(int size) {
                return new Glumac[size];
            }

    };
        protected  Glumac(Parcel in)
        {
            ime = in.readString();
            prezime = in.readString();
            godinaRodjenja = in.readString();
            mjestoRodjenja = in.readString();
            godinaSmrti = in.readString();
            link = in.readString();
            biografija = in.readString();
            spol = in.readString();
            slika = in.readString();
            rating = in.readString();

        }
    @Override
    public int describeContents() {
        return 0;
    }



    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ime);
        dest.writeString(prezime);
        dest.writeString(godinaRodjenja);
        dest.writeString(godinaSmrti);
        dest.writeString(spol);
        dest.writeString(link);
        dest.writeString(mjestoRodjenja);
        dest.writeString(rating);
        dest.writeString(slika);
        dest.writeString(biografija);

    }
}

