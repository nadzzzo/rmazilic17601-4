package ba.unsa.nzilic1etf.spirala;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class Main3Activity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        Resources res = getResources();

        Button dugmeGlumci = (Button) findViewById(R.id.button2GLUMCI);
        Button dugmeZanrovi = (Button) findViewById(R.id.button2ZANROVI);
        ListView lista = (ListView) findViewById(R.id.listViewREZISERI);

        final ArrayList<Reziser> reziseri = new ArrayList<Reziser>();
        reziseri.add(new Reziser("Nadja", "Zilic"));
        reziseri.add(new Reziser("Michael","Mann"));
        reziseri.add(new Reziser("Steven","Spielberg"));
        reziseri.add(new Reziser("Richard","Donner"));
        reziseri.add(new Reziser("Woody","Allen"));
        reziseri.add(new Reziser("John","August"));

        final AdapterListeRezisera adapter = new AdapterListeRezisera(this, reziseri, this.getResources());
        lista.setAdapter(adapter);


        dugmeGlumci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentR = new Intent(Main3Activity.this, MainActivity.class);
                startActivity(intentR);
            }
        });

        dugmeZanrovi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentZ = new Intent(Main3Activity.this, Main4Activity.class);
                startActivity(intentZ);
            }
        });

    }
    class Adapter1 extends ArrayAdapter<String>{
        Context contekst;

        String[] imena;
        String[] prezimena;


        Adapter1(Context c, String[] im, String[] pr)
        {
            super(c,R.layout.reziserulisti,R.id.tekstViewPR,pr);
            this.contekst = c;
            this.imena = im;

            this.prezimena = pr;

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            LayoutInflater inf = (LayoutInflater) contekst.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View red = inf.inflate(R.layout.reziserulisti, parent, false);
            TextView prikazimena = (TextView) red.findViewById(R.id.tekstViewIR);
            TextView prikazprezimena = (TextView) red.findViewById(R.id.tekstViewPR);

            prikazimena.setText(imena[position]);
            prikazprezimena.setText(prezimena[position]);



            return red;
        }
    }
}

