package ba.unsa.nzilic1etf.spirala;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by User on 9.6.2017.
 */

public class MyIntentServiceFilmovi extends IntentService {

    final public static int STATUS_RUNNING = 0;
    final public static int STATUS_FINISHED = 1;
    final public static int STATUS_ERROR = 2;
    ArrayList<String> filmovi_staticka = new ArrayList<>();
    boolean citajizbaze = false;

    public MyIntentServiceFilmovi(String name) {
        super(name);
    }

    public MyIntentServiceFilmovi() {
        super(null);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override

    protected void onHandleIntent(Intent intent) {

        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        Bundle bundle = new Bundle();

        final String imefilma = intent.getStringExtra("ime");
        //receiver.send(STATUS_RUNNING,Bundle.EMPTY);

        String query = "";
        try {
            query = URLEncoder.encode(imefilma, "utf-8");
            if(query.contains("actor:")) { citajizbaze = true; }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // tring url1 = "https://api.themoviedb.org/3/search/person?api_key=04238c25b41882a87b8d61e6d14cbb6d&query=" + query;
        String url1 = "https://api.themoviedb.org/3/search/movie?api_key=04238c25b41882a87b8d61e6d14cbb6d&query=" + query;

        try {
            URL url = new URL(url1);

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONArray items = jo.getJSONArray("results");
            receiver.send(STATUS_RUNNING, bundle);

            for (int i = 0; i < items.length(); i++) {

                //trazimo filmove po id.u

                JSONObject jedanfilm = items.getJSONObject(i);
                String naziv = jedanfilm.getString("original_title");
                filmovi_staticka.add(naziv);

            }
            bundle.putStringArrayList("WebListaFilmova", filmovi_staticka);
            receiver.send(STATUS_FINISHED, bundle);

        } catch (MalformedURLException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
            e.printStackTrace();
        } catch (IOException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
            e.printStackTrace();
        } catch (JSONException je) {
            bundle.putString(Intent.EXTRA_TEXT, je.toString());
            receiver.send(STATUS_ERROR, bundle);
            je.printStackTrace();
        }
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }


}

