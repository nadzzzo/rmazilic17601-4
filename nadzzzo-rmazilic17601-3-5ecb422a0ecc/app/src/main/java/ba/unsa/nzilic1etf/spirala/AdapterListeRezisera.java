package ba.unsa.nzilic1etf.spirala;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by User on 28.3.2017.
 */

public class AdapterListeRezisera extends BaseAdapter {
    private Activity aktivnost;
    private ArrayList podaci;
    private static LayoutInflater inflater = null;
    public Resources res;
    Reziser privVrijednosti = null;
    int i = 0;

    public AdapterListeRezisera(Activity a, ArrayList e, Resources res) {
        this.aktivnost = a;
        this.podaci = e;
        this.res = res;
        inflater = (LayoutInflater) aktivnost.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        if (podaci.size() <= 0) return 1;
        return podaci.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class PrikazReda {

        public TextView tekstime;
        public TextView tekstprezime;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View pogled = convertView;
        PrikazReda prikaz;

        if (convertView == null) {
            pogled = inflater.inflate(R.layout.reziserulisti, null);

            prikaz = new PrikazReda();

            prikaz.tekstime = (TextView) pogled.findViewById(R.id.tekstViewIR);
            prikaz.tekstprezime = (TextView) pogled.findViewById(R.id.tekstViewPR);


            pogled.setTag(prikaz);

        } else prikaz = (PrikazReda) pogled.getTag();

        if (podaci.size() <= 0) {
            prikaz.tekstime.setText("No data");

        } else {
            privVrijednosti = null;
            privVrijednosti = (Reziser) podaci.get(position);
            prikaz.tekstime.setText(privVrijednosti.getIme());
            prikaz.tekstprezime.setText(privVrijednosti.getPrezime());


        }
        return pogled;
    }
}
