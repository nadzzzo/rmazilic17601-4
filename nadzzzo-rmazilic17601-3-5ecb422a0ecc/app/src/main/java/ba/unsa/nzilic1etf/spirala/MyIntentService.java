package ba.unsa.nzilic1etf.spirala;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import static ba.unsa.nzilic1etf.spirala.DataBaseAdapter.db;

/**
 * Created by User on 22.5.2017.
 */

public class MyIntentService extends IntentService {

    final public static int STATUS_RUNNING=0;
    final public static int STATUS_FINISHED=1;
    final public static int STATUS_ERROR=2;

    Glumac g = new Glumac();
     public ArrayList<Glumac> glumci = new ArrayList<>();
    public ArrayList<Reziser> reziseri = new ArrayList<>();
    public ArrayList<String> idFilmova = new ArrayList<>();
    public ArrayList<Zanr> zanrovi = new ArrayList<>();
    Boolean baza = false, reziser = false;

     Integer idGlumca;

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }



    public MyIntentService()
    {
        super(null);
    }

    public MyIntentService(String name)
    {
        super(name);
    }
    @Override
    public void onCreate()
    {
        super.onCreate();

    }

    @Override
    protected void onHandleIntent(Intent intent) {

        //         ResultReceiver reciever = intent.getParcelableExtra(MyReciever.RESULT_RECEIEVER_EXTRA);

        final ResultReceiver resiver = intent.getParcelableExtra("resiver");
        Bundle bundle = new Bundle();
        final String ime_koje_trazim = intent.getStringExtra("ime");

        resiver.send(STATUS_RUNNING, bundle);
        String query = null;
        try {

            if (ime_koje_trazim.contains("actor:")) baza = true;
            else if (ime_koje_trazim.contains("director:")) reziser = true;
            else query = URLEncoder.encode(ime_koje_trazim, "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        }


            String url1 = "https://api.themoviedb.org/3/search/person?api_key=04238c25b41882a87b8d61e6d14cbb6d&query=" + query;

            try {
                URL url = new URL(url1);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("User-Agent", "Chrome");
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                String rezultat = convertStreamToString(in);
                JSONObject jo = new JSONObject(rezultat);
                JSONArray items = jo.getJSONArray("results");
                resiver.send(STATUS_RUNNING, bundle);

                //**************TRAŽIMO GLUMCE
                for (int i = 0; i < 8; i++) {

                    try {
                        JSONObject artist = items.getJSONObject(i);

                        idGlumca = artist.getInt("id");
                        String glumac = "https://api.themoviedb.org/3/person/" + idGlumca + "?api_key=04238c25b41882a87b8d61e6d14cbb6d";

                        URL urlglumca = new URL(glumac);
                        HttpURLConnection urlll = (HttpURLConnection) urlglumca.openConnection();
                        InputStream input = new BufferedInputStream(urlll.getInputStream());
                        String rezglumca = convertStreamToString(input);
                        JSONObject glum = new JSONObject(rezglumca);
                        String ime = glum.getString("name");
                        String datumro = glum.getString("birthday");
                        String bio = glum.getString("biography");
                        String datumsmrti = glum.getString("deathday");
                        String godrodj = "";

                        if (datumro.length() > 4) godrodj = datumro.substring(0, 4);
                        String mjestorodjenja = glum.getString("place_of_birth");
                        Integer pol = glum.getInt("gender");
                        Integer rating = glum.getInt("popularity");

                        String imdb = glum.getString("imdb_id");
                        String link = glum.getString("profile_path");
                        String slika = link.substring(1);
                        String spol = "";
                        if (pol == 1) spol = "Female";
                        else if (pol == 2) spol = "Male";

                        String idGlumcaa = idGlumca.toString();

                        String imegl =ime.substring(0, ime.indexOf(" "));
                        int razmak = ime.indexOf(" ")+1;
                        String prez= ime.substring(razmak);

                        Glumac g = new Glumac(slika, ime, "", godrodj, mjestorodjenja, rating.toString(), spol, datumsmrti, bio, imdb);
                        idFilmova.clear();
                        // glumci.add(g);
                        //ZA SVAKOG GLUMCA NAĆI SVE FILMOVE
                        String url_glumac = "https://api.themoviedb.org/3/discover/movie?" + "with_cast=" + idGlumcaa + "&sort_by=release_date.desc&api_key=04238c25b41882a87b8d61e6d14cbb6d";

                        try {
                            URL novaurl = new URL(url_glumac);
                            HttpURLConnection urlConnection2 = (HttpURLConnection) novaurl.openConnection();
                            InputStream in2 = new BufferedInputStream(urlConnection2.getInputStream());

                            String rez = convertStreamToString(in2);

                            JSONObject joj = new JSONObject(rez);
                            JSONArray glumci_json = joj.getJSONArray("results");

                            for (int z = 0; z < glumci_json.length(); z++) {
                                JSONObject item = glumci_json.getJSONObject(z);

                                String id = item.getString("id");
                                if (id.isEmpty() || id.equals("") || id.equals("null") || id == null) {

                                } else {
                                    idFilmova.add(id);
                                }


                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                        //********ZA SVAKI FILM NAĆI REŽISERE
                        try {
                            reziseri.clear();
                            for (int r = 0; r < idFilmova.size(); r++) {

                                String url_glumac2 = "https://api.themoviedb.org/3/movie/" + idFilmova.get(r) + "/credits?api_key=04238c25b41882a87b8d61e6d14cbb6d";

                                URL url3 = new URL(url_glumac2);
                                HttpURLConnection urlConnection3 = (HttpURLConnection) url3.openConnection();
                                InputStream in3 = new BufferedInputStream(urlConnection3.getInputStream());

                                String rez = convertStreamToString(in3);

                                JSONObject jo3 = new JSONObject(rez);
                                JSONArray glumci_json = jo3.getJSONArray("crew");

                                for (int i2 = 0; i2 < glumci_json.length(); i2++) {

                                    JSONObject item = glumci_json.getJSONObject(i2);

                                    if (item.getString("job").equals("Director"))
                                        if (!provjeri_sadrzi_li(reziseri, item.getString("name"))) {
                                            Reziser ubaci = new Reziser(item.getString("name"), "");
                                            reziseri.add(ubaci);
                                        }
                                    if (reziseri.size() == 7) break;
                                }
                                if (reziseri.size() == 7) break;
                            }
                            g.dodajRezisere(reziseri);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //završeno dodavanje režisera


                        //DODAVANJE ŽANROVA

                        zanrovi.clear();
                        for (int p = 0; p < idFilmova.size(); p++) {

                            try {
                                String url_glumac2 = "https://api.themoviedb.org/3/movie/" + idFilmova.get(p) + "?api_key=04238c25b41882a87b8d61e6d14cbb6d";

                                URL url4 = new URL(url_glumac2);
                                HttpURLConnection urlConnection4 = (HttpURLConnection) url4.openConnection();
                                InputStream in4 = new BufferedInputStream(urlConnection4.getInputStream());

                                String rez = convertStreamToString(in4);

                                JSONObject jo4 = new JSONObject(rez);
                                JSONArray glumci_json = jo4.getJSONArray("genres");

                                for (int i3 = 0; i3 < glumci_json.length(); i3++) {

                                    JSONObject item = glumci_json.getJSONObject(i3);

                                    if (!provjeri_sadrzi_li_zanrovi(zanrovi, item.getString("name"))) {

                                        Zanr ubaci = new Zanr(R.drawable.action, item.getString("name"));
                                        zanrovi.add(ubaci);
                                        if (zanrovi.size() == 7) break;

                                    }

                                    if (zanrovi.size() == 7) break;
                                }
                                if (zanrovi.size() == 7) break;


                                //g.dodajZanrove(zanrovi);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        g.dodajZanrove(zanrovi);
                        glumci.add(g);
                    } catch (Exception er) {
                    }
                }

                bundle.putParcelableArrayList("ListaGlumaca", glumci);
                resiver.send(STATUS_FINISHED, bundle);


            } catch (Exception e) {
                e.printStackTrace();

            }

        //ako je pretraga sa INTERNETA



            /*String koga = ime_koje_trazim.substring(6);
            String sql ="SELECT * FROM "+
                    DataBaseAdapter.DATABASE_TABLE1 +
                    " WHERE " + DataBaseAdapter.KEY_NAME + " LIKE '%" + koga + "%'";


            Cursor cursor = MainActivity.db.getReadableDatabase().rawQuery(sql, null);
            cursor.moveToFirst();
            while (cursor.moveToNext()){
                String ko = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_NAME));
                String spol = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_GENDER));
                String prezime = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_LASTNAME));
                String mjesto = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_BIRTHPLACE));
                String link = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_IMDB));
                String biografija = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_BIOGRAPHY));
                String rank = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_RATING));
                String slika = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_PICTURE));
                String rod = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_BIRTHDAY));
                String umr = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_DEATHDAY));
                //ArrayList<String> zanr = cursor.getColumnIndexOrThrow(DataBaseAdapter.KEY_GENDER);
                Integer gid = cursor.getInt(cursor.getColumnIndex(DataBaseAdapter.KEY_personID));

                Glumac gl = new Glumac(slika,ko, prezime,rod,mjesto,rank,spol,umr,biografija,link);
                glumci.add(gl);
            }
            cursor.close();
            */
            //bundle.putString("query", intent.getExtras().getString("query"));
           /* String koga = ime_koje_trazim.substring(6);
            String[] koloneRezultat = new String[]{
                    DataBaseAdapter.KEY_personID,
                    DataBaseAdapter.KEY_NAME,
                    DataBaseAdapter.KEY_LASTNAME,
                    DataBaseAdapter.KEY_BIRTHDAY,
                    DataBaseAdapter.KEY_DEATHDAY,
                    DataBaseAdapter.KEY_BIOGRAPHY,
                    DataBaseAdapter.KEY_BIRTHPLACE,
                    DataBaseAdapter.KEY_GENDER,
                    DataBaseAdapter.KEY_IMDB ,
                    DataBaseAdapter.KEY_PICTURE,
                    DataBaseAdapter.KEY_RATING,


            };
            String where = DataBaseAdapter.KEY_NAME + " LIKE '%" + query + "%'" ;


            SQLiteDatabase database = MainActivity.db.getReadableDatabase();
            Cursor cursor = database.query(MainActivity.db.getDatabaseName(),koloneRezultat,where,null,null,null,null);
            cursor.moveToFirst();
            while (cursor.moveToNext()){
                String ko = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_NAME));
                String spol = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_GENDER));
                String prezime = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_LASTNAME));
                String mjesto = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_BIRTHPLACE));
                String link = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_IMDB));
                String biografija = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_BIOGRAPHY));
                String rank = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_RATING));
                String slika = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_PICTURE));
                String rod = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_BIRTHDAY));
                String umr = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_DEATHDAY));
                //ArrayList<String> zanr = cursor.getColumnIndexOrThrow(DataBaseAdapter.KEY_GENDER);
                Integer gid = cursor.getInt(cursor.getColumnIndex(DataBaseAdapter.KEY_personID));

                Glumac gl = new Glumac(slika,ko, prezime,rod,mjesto,rank,spol,umr,biografija,link);
                glumci.add(gl);
            }
            cursor.close();

            //String where = DataBaseAdapter.KEY_NAME + " LIKE '%" + query + "%'" ;

            bundle.putParcelableArrayList("ListaGlumaca",    glumci);
            //bundle.putBoolean("koga", false);
            resiver.send(STATUS_FINISHED, bundle);
        }
        */
    }
    private boolean provjeri_sadrzi_li_zanrovi(ArrayList<Zanr> zanrovi, String name) {
        for(int i=0;i<zanrovi.size();i++) {
            if(zanrovi.get(i).getNaziv().equals(name)) return true;
        }
        return  false;
    }


    private boolean provjeri_sadrzi_li(ArrayList<Reziser> reziseri, String name) {
        for(int i=0;i<reziseri.size();i++) {
            String imeprezime = reziseri.get(i).getIme() + " " + reziseri.get(i).getPrezime();

            if(imeprezime.equals(name)) return true;
        }
        return  false;
    }

   /* public citanjeizbaze(String query)
    {
        String koga = query.substring(6);
        String sql ="SELECT * FROM "+DataBaseAdapter.DATABASE_TABLE1 +" WHERE " + DataBaseAdapter.KEY_NAME + " LIKE '%" + koga + "%'";
        //  Cursor cursor = pocetni.db.getReadableDatabase().rawQuery(sql, null);


        DataBaseAdapter dbEntry = new DataBaseAdapter(context);
        SQLiteDatabase db = dbEntry.getWritableDataBase();
        dbEntry.open();

        Cursor cursor= dbEntry.getAllActors();
        cursor.moveToFirst();
        while (cursor.moveToNext()){
            String ko = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_NAME));
            String spol = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_GENDER));
            String prezime = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_LASTNAME));
            String mjesto = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_BIRTHPLACE));
            String link = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_IMDB));
            String biografija = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_BIOGRAPHY));
            String rank = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_RATING));
            String slika = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_PICTURE));
            String rod = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_BIRTHDAY));
            String umr = cursor.getString(cursor.getColumnIndex(DataBaseAdapter.KEY_DEATHDAY));
            //ArrayList<String> zanr = cursor.getColumnIndexOrThrow(DataBaseAdapter.KEY_GENDER);
            Integer gid = cursor.getInt(cursor.getColumnIndex(DataBaseAdapter.KEY_personID));

        public Glumac(String slika, String ime,String prezime, String godinaRodj,
        String mjestoRodj, String rating, String spol, String godinaSmr, String bio, String link)
            Glumac gl = new Glumac(slika,ko, prezime,rod,mjesto,rank,spol,umr,biografija,link);
            glumci.add(gl);
        }
        cursor.close();
        b.putString("query", intent.getExtras().getString("query"));
        b.putSerializable("glumci", (Serializable) glumci);
        b.putBoolean("koga", false);
        receiver.send(STATUS_FINISHED, b);
    } */



}
