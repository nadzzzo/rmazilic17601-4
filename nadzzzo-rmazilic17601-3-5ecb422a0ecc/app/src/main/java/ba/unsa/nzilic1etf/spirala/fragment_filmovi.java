package ba.unsa.nzilic1etf.spirala;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by User on 9.6.2017.
 */

public class fragment_filmovi extends Fragment implements ResiverZaRezultat.Receiver {

    OnItemClick oic;
    ArrayAdapter<String> aa;
    ArrayList<String> filmovi = new ArrayList<>();
    ResiverZaRezultat mReceiver;

    public interface OnItemClick {
        public void onItemClicked1(int pos);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.filmovifragment, container, false);

        //ovo je trebalo uraditi...
        if (getArguments() != null && getArguments().containsKey("filmovi"))
            filmovi = getArguments().getStringArrayList("filmovi");
        ListView l = (ListView) v.findViewById(R.id.listViewFilmovi);
        Button pretrazi = (Button) v.findViewById(R.id.button5);
        final EditText ime = (EditText) v.findViewById(R.id.editText2);
        aa = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, filmovi);
        l.setAdapter(aa);

        try {
            oic = (OnItemClick) getActivity();
        } catch (ClassCastException ex) {
            throw new ClassCastException(getActivity().toString() + "Treba implementirati OnItemClick");
        }

        // final Poruka po=null;

        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                oic.onItemClicked1(position);
            }
        });

        pretrazi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), MyIntentServiceFilmovi.class);

                mReceiver = new ResiverZaRezultat(new Handler());
                mReceiver.setReceiver(fragment_filmovi.this);
                String imeglumca = ime.getText().toString();
                intent.putExtra("ime", imeglumca);
                intent.putExtra("receiver", mReceiver);
                getActivity().startService(intent);
                //   po.porukica(getActivity(), "JESAM");

            }
        });

        return v;
    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case MyIntentService.STATUS_RUNNING:

                break;
            case MyIntentService.STATUS_FINISHED:

                /* Dohvatanje rezultata i update UI */
                //   String[] results = resultData.getStringArray("result");
                filmovi.clear();
                ArrayList<String> resultati = resultData.getStringArrayList("WebListaFilmova");
                for (String m : resultati) {
                    filmovi.add(m);
                }
                aa.notifyDataSetChanged();
                break;
            case MyIntentService.STATUS_ERROR:

                /* Slučaj kada je došlo do greške */
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                break;
        }
    }
}
