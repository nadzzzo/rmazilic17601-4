package ba.unsa.nzilic1etf.spirala;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Locale;

/**
 * Created by User on 10.4.2017.
 */

public class fragment_dugmad extends Fragment implements View.OnClickListener {

    private FragmentChangeListener kom;




    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle  savedInstanceState)
    {
        View view = inflater.inflate(R.layout.dugmad_fragment, container, false);
        kom = (FragmentChangeListener)getActivity();
        String jezik = "";
        //  Resources.getSystem().getConfiguration().locale;



        Button glumci = (Button)view.findViewById(R.id.button);
        Button reziseri = (Button)view.findViewById(R.id.button2);
        Button zanrovi = (Button)view.findViewById(R.id.button3);
        Button filmovi = (Button)view.findViewById(R.id.button7);
        ImageView slika = (ImageView)view.findViewById(R.id.imageView2);

        jezik =Locale.getDefault().getDisplayLanguage();

        glumci.setOnClickListener(this);
        reziseri.setOnClickListener(this);
        zanrovi.setOnClickListener(this);
        filmovi.setOnClickListener(this);

        //slika.setImageResource(R.drawable.uk);
        if(jezik.equals("English")) {
            slika.setImageResource(R.drawable.uk);
        }
        else if(jezik.equals("Croatian")) // stavila sam hrvatski jer na mobu nemam ponudjen bosanski pa samo da mogu provjerit
        {
            slika.setImageResource(R.drawable.bih);
        }

        return view;
    }
    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.button:
                kom.onButtonClicked​("1");
                break;
            case R.id.button2:
                kom.onButtonClicked​("2");
                break;
            case R.id.button3:
                kom.onButtonClicked​("3");
                break;
            case R.id.button7:
                kom.onButtonClicked​("4");
                break;
            default:
                throw new RuntimeException("Unknown button ID");
        }

    }





    }


