package ba.unsa.nzilic1etf.spirala;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by User on 28.3.2017.
 */

public class Reziser implements Parcelable {
    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    private String ime;

    public String getPrezime() {
        return prezime;
    }



    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    private String prezime;

    public Reziser(String i, String p){
        this.ime=i;
        this.prezime=p;
    }
    public static final Creator<Reziser> CREATOR = new Creator<Reziser>() {
        @Override
        public Reziser createFromParcel(Parcel in)
        {
            return new Reziser(in);
        }

        @Override
        public Reziser[] newArray(int size) {
            return new Reziser[size];
        }

    };
    protected  Reziser(Parcel in) {
        ime = in.readString();
        prezime = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ime);
        dest.writeString(prezime);
    }
}
